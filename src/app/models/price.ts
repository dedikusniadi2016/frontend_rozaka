export class Price {
    id: number;
    title: string;
    price: string;
    description: string;
}
