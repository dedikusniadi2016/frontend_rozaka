import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ApiService } from '../api.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  PriceData: any;

  constructor(private router: Router, public apiService: ApiService) {
    this.PriceData = [];
  }

  ngOnInit() {
    this.getPrice();
  }

  getPrice() {
    this.apiService.getList().subscribe(response => {
      console.log(response);
      this.PriceData = response;
    })
  }
 
  btnClick(){
    this.router.navigateByUrl('/pos');
    console.log(this.router.navigateByUrl('/pos'));

}


}
